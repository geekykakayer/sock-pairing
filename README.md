# Sock Pairing

A tool that runs Monte Carlo on two sock paring strategies:

# Strategies

## StephenPairing

Pair socks as they come out the drawer

## TimPairing

Pair socks as they come out the wash

# Results

```
./socks.py --days 10000000
StephenPairing: 2.1107695 tries per day
TimPairing: 2.1106217 tries per day
```

Inconclusive...
