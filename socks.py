#!/usr/bin/env python3
import random
import argparse
import csv

from enum import Enum
from typing import List
from pathlib import Path

SCRIPT_DIR = Path(__file__).parent
COLORS_FILE = SCRIPT_DIR / 'colors.csv'
COLORS = { l[0] : l[1:] for l in list(csv.reader(COLORS_FILE.open())) }

class SockPairing:
    def __init__(self, num_pairs = 10, percent_match = 0.8, identical_non_matching = True):
        # Sock storage
        self.in_drawer = []
        self.dirty = []

        # This many need to match
        num_matching = int(num_pairs * percent_match)

        def get_color(pair_id):
            if pair_id < num_matching:
                return "Red" # Lots of this kind
            
            if identical_non_matching:
                return "Green" # Non natching all the same

            else:
                return random.choice(list(COLORS.keys()))
   
        # Keep adding socks of same color until enough match
        for i in range(num_pairs):
            pair_color = get_color(i)
            self.in_drawer += [pair_color, pair_color]

    def _search_for_pair_(self, list_of_socks : List[str]) -> (int, (str, str)):
        """ Searches a list, removes two matching items
            Returns the number of pairing attempts and the pair """

        # Grab a random sock
        first_sock = random.choice(list_of_socks)
        list_of_socks.remove(first_sock)

        # Keep trying to pair
        tries = 0
        while True:
            # Grab a random second sock
            tries += 1
            second_sock = random.choice(list_of_socks)

            # If pair matches, return it
            if second_sock == first_sock:
                list_of_socks.remove(second_sock)
                return tries, (first_sock, second_sock)

    def wear_pair(self) -> int:
        """ Return the number of pairings to put on a pair of socks
            Automatically wash if you run out of socks              """

        #If not enough socks, do a wash
        wash_tries = 0
        if len(self.in_drawer) < 2:
            wash_tries += self._wash_socks_()

        #Find a pair from drawer
        find_tries = self._pair_from_drawer_()

        #Return tries
        return wash_tries + find_tries

    def _wash_socks_(self) -> int:
        """ Return the number of pairing attempts when washing socks """
        raise Exception

    def _pair_from_drawer_(self) -> int:
        """ Return the number of pairing attempts when getting socks from drawer """
        raise Exception

class TimPairing(SockPairing): 
    def _wash_socks_(self):
        # Loop through pairs of dirty socks

        # Check for odd socks...
        if (len(self.dirty) % 2):
            raise Exception

        tries = 0
        for i in range(int(len(self.dirty) / 2)):

            # Find a pair and put in drawer
            pair_tries, pair = self._search_for_pair_(self.dirty)
            for sock in pair:
                self.in_drawer.append(sock)
            tries += pair_tries

        return tries

    def _pair_from_drawer_(self) -> int:
        # Just grab a pair

        first_sock = random.choice(self.in_drawer)
        self.in_drawer.remove(first_sock)
        self.in_drawer.remove(first_sock)

        self.dirty.append(first_sock)
        self.dirty.append(first_sock)

        return 0

class StephenPairing(SockPairing):
    def _wash_socks_(self):
        # Just put in drawer

        self.in_drawer += self.dirty
        self.dirty = []
        return 0

    def _pair_from_drawer_(self):
        #Find a pair and put in dirty

        tries, pair =  self._search_for_pair_(self.in_drawer)
        for sock in pair:
            self.dirty.append(sock)

        return tries


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--days", type=int, default=1000)
    parser.add_argument("--socks", type=int, default=20)
    parser.add_argument("--matching", type=float, default=0.8)
    args = parser.parse_args()


    for strategy in [ StephenPairing, TimPairing ]:
        socks = strategy(args.socks, args.matching)

        tries = 0
        for i in range(args.days):
            tries += socks.wear_pair()
        print(f"{type(socks).__name__}: {tries / args.days} tries per day")
